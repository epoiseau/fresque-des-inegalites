---
fromCardId: '27'
toCardId: '23'
status: valid
---
L'emploi informel ne permet pas en général d'accéder aux prestations sociales, car ces dernières sont souvent conditionnées par des cotisations préalables (chômage, retraite, accidents professionnels...) qui ne sont pas versées dans le secteur informel.
