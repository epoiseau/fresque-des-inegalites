---
fromCardId: '45'
toCardId: '42'
status: valid
---
La politique agricole est liée aux tarifs douaniers qui sont ajustés : 
- à une valeur élevée pour éviter la concurrence avec les productions réalisées sur le sol national (protectionnisme), par exemple aux USA pour le maïs, le soja et le blé, mais aussi en ce qui concerne les produits transformés,
- à une valeur faible pour réduire le coût de l'alimentation pour les consommateurs, notamment pour les productions importées, par exemple en Europe le riz et le sucre.
