---
fromCardId: '20'
toCardId: '36'
status: valid
---
La diversité et l'indépendance des médias est l'un des critères qui permettent de définir l'indice de démocratie. Lorsqu'une grande partie de l'audience est exposée aux seules idées promues par des intérêts financiers et industriels, l'espace de débat démocratique se réduit.
