---
fromCardId: '37'
toCardId: '3'
status: valid
---
Les gros donateurs, qui financent les candidats les plus libéraux, espèrent-ils en retour une baisse de leur imposition ? Il est difficile de répondre à cette question en absence de transparence des dons, mais il est assez troublant de constater qu'abaisser la fiscalité des plus riches fait partie des toutes premières mesures mises en oeuvre lorsque ces candidats accèdent au pouvoir.
