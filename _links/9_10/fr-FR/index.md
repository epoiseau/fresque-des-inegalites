---
fromCardId: '9'
toCardId: '10'
status: valid
---
Lorsque les impôts des entreprises baissent, le reste des ménages est susceptible de voir sa charge fiscale augmenter pour conserver des recettes fiscales constantes. 

