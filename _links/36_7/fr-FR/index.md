---
fromCardId: '36'
toCardId: '7'
status: valid
---
Les acquis démocratiques (la règlementation politique et juridique) sont un facteur de régulation imposés aux forces aveugles du marché, inversement le recul démocratique est une opportunité pour la dérèglementation.
