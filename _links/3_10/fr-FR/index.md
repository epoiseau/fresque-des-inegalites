---
fromCardId: '3'
toCardId: '10'
status: valid
---
Lorsque les impôts des plus riches baissent, le reste des ménages est susceptible d'être proportionnellement davantage taxé pour conserver des recettes fiscales constantes. Cela peut prendre la forme d'un transfert de l'impôt sur le revenu, qui est progressif, vers la TVA, qui s'applique de la même manière à toutes et tous.
