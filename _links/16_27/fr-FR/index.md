---
fromCardId: '16'
toCardId: '27'
status: valid
---
Les ajustements structurels ont des conséquences économiques sensées être favorables à long terme (création de nouvelles activités dans le secteur privé), mais qui restent hypothétiques. Ils ont en revanche des conséquences sociales immédiates et certaines : compression d'effectifs dans le secteur public, augmentation du coût de la vie, diminution des aides et subventions. Ces mesures poussent les personnes à trouver des solutions dans le secteur informel dans une logique de survie économique et de débrouillardise, ce qui augmente les effectifs de l'emploi informel.
