---
fromCardId: '31'
toCardId: '35'
status: valid
---
Les épisodes de catastrophes découlant du changement climatique sont déjà à l'origine du déplacement de millions de personnes chaque année. De lents changements dans l’environnement, tels que l’acidification des océans, la désertification et l’érosion des côtes, ont également un impact direct sur les moyens de subsistance des populations et sur leur capacité à survivre dans leur lieu d’origine. Le GIEC prévoit que les changements induits par la crise climatique risquent d’influencer les schémas migratoires. 

