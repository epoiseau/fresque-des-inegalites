---
fromCardId: '7'
toCardId: '5'
status: valid
---
Lorsque la dérégulation est forte, les 1 % des plus riches sont en mesure d’accaparer une forte proportion des revenus nationaux.
