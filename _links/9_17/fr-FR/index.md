---
fromCardId: '9'
toCardId: '17'
status: valid
---
La baisse de l'impôt sur les entreprises constitue une perte de recettes pour les États, et contribue au déficit budgétaire, lui-même financé par de la dette. 
