---
fromCardId: '17'
toCardId: '15'
status: valid
---
Le paiement des intérêts de la dette publique est une charge budgétaire obligatoire, qui est réalisée au détriment des politiques publiques d'accès aux services essentiels, comme les soins de santé.
