---
fromCardId: '45'
toCardId: '43'
status: valid
---
La politique agricole favorise la concentration des acteurs de l'agroindustrie de plusieurs manières : 
- les subventions sont généralement proportionnelle à la quantité produite ou à la surface cultivée, ce qui renforce l'avantage concurrentiel des grandes exploitations au détriment des petites, car elles bénéficient à la fois de ces subventions et des économies d'échelle,
- la réglementation dans l'industrie agroalimentaire, visant à protéger l'environnement et la santé, crée des barrières pour les petits acteurs, qui n'ont pas la capacité de mettre en place les mesures demandées (traçabilité par exemple), et favorise donc mécaniquement les grandes entreprises.
