---
fromCardId: '1'
toCardId: '20'
status: valid
---
Les personnes les plus riches [en patrimoine] détiennent près de la moitié des richesses de la planète, il est logique qu'ils détiennent une part importante des médias également.
