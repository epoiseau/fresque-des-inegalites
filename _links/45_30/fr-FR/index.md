---
fromCardId: '45'
toCardId: '30'
status: valid
---
L’aggravation de la faim dans le monde est en partie le résultat d’un échec à résoudre les problèmes structurels du système agricole et alimentaire mondial. Un système basé sur la prévalence de l’industrie, qui concentre pouvoir et richesses entre les mains d’un petit nombre d’acteurs et génère beaucoup d’inégalités. Le modèle agro-industriel épuise les terres et rend l’agriculture de plus en plus vulnérable aux changements climatiques. Il crée une concurrence dévastatrice pour les millions de petites exploitations familiales, écrasées par le marché, et fait plonger dans la pauvreté, les paysans, et en particulier les paysannes, qui sont celles qui souffrent le plus de la faim.

