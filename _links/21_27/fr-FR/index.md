---
fromCardId: '21'
toCardId: '27'
status: valid
---
Des marchandes ambulantes et travailleuses domestiques aux agricultrices de subsistance et aux travailleuses agricoles saisonnières, les femmes représentent une part disproportionnée des travailleurs du secteur non structuré, car elles peuvent moins facilement accéder aux emplois de meilleure qualité.
