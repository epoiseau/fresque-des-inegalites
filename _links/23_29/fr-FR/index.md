---
fromCardId: '23'
toCardId: '29'
status: valid
---
L'absence de sécurité sociale est particulièrement préjudiciable aux personnes qui ont un patrimoine faible à nul, car en cas d'accident de vie (maladie, perte d'emploi...) elles n'auront pas de réserve leur permettant d'y faire face. Elles seront ainsi contraintes à rester dans la pauvreté.
