---
fromCardId: '24'
toCardId: '2'
status: valid
---
L'effectif de la population mondiale vivant avec moins de 10 $PPA/j est alimenté par les travailleurs pauvres ainsi que par les personnes qui sont à la charge de ces travailleurs pauvres.
