---
fromCardId: '7'
toCardId: '14'
status: valid
---
La dérégulation financière favorise la suppression des obstacles à la liberté de circulation des capitaux (douane, contrôle des mouvements de capitaux, souveraineté nationale...) Cela facilite l'accès aux paradis fiscaux pour la gestion des capitaux.
