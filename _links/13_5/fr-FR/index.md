---
fromCardId: '13'
toCardId: '5'
status: valid
---
Les augmentations de rémunération des hauts dirigeants, et du capital sous forme de dividendes, profitent davantage à celles et ceux qui sont déjà les plus riches en patrimoine.
