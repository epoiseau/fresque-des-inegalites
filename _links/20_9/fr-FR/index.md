---
fromCardId: '20'
toCardId: '9'
status: valid
---
Les médias détenus par les groupes industriels et financiers promeuvent dans l'opinion l'idée que réduire les charges pesant sur les entreprises (dont les charges fiscales) est favorable au développement économique, en occultant les impacts écologiques et sociaux.
