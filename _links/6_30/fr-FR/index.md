---
fromCardId: '6'
toCardId: '30'
status: valid
---
Les crises économiques peuvent contribuer à l'insécurité alimentaire, par divers facteurs tels que l'augmentation du prix des denrées, la baisse du revenu des populations, la hausse du prix des carburants, la perturbation des circuits d'approvisionnement...
