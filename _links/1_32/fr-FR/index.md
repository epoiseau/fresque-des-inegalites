---
fromCardId: '1'
toCardId: '32'
status: valid
---
Les inégalités de patrimoine impliquent des inégalités d'émissions. Les personnes les plus riches sont responsables d'une grande partie des émissions en raison d'un mode de vie plus émetteur de CO2 que les personnes les plus pauvres.
