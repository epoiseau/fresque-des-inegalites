---
fromCardId: '14'
toCardId: '17'
status: valid
---
Les paradis fiscaux privent les États de recettes fiscales. Cela crée un déficit budgétaire qui est financé par de la dette publique.
