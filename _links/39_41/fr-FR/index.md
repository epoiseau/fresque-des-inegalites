---
fromCardId: '39'
toCardId: '41'
status: valid
---
- Les stéréotypes sur les rôles traditionnels des hommes et des femmes attribuent aux femmes la responsabilité des soins aux enfants, des personnes âgées et des personnes handicapées. Ces stéréotypes peuvent conduire à ce que les femmes soient socialisées dès leur plus jeune âge à prendre en charge ces responsabilités.
- Les stéréotypes sur les compétences et les qualités des femmes peuvent conduire à ce que les femmes soient perçues comme étant plus naturellement douées pour les tâches de soins. Ces stéréotypes peuvent contribuer à la perpétuation de la division genrée du travail, dans laquelle les femmes sont les principales responsables des soins non rémunérés.
- Les politiques publiques peuvent également contribuer à renforcer les stéréotypes de genre sur le travail de soins non rémunéré. Par exemple, les politiques qui ne soutiennent pas suffisamment les services de garde d'enfants peuvent rendre plus difficile pour les femmes de concilier leur vie professionnelle et leur vie familiale. Cela peut conduire à ce que les femmes assument une plus grande part des responsabilités de soins non rémunérés.  
- Les femmes sont donc confrontées à un faisceau d'attentes sociales qui les poussent à accepter d'assumer la responsabilité des soins non rémunérés. 

