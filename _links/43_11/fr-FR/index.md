---
fromCardId: '43'
toCardId: '11'
status: valid
---
La forte concentration des chaînes de production de l'agroindustrie est une source de profit qui sont redistribués aux actionnaires en dividendes, au détriment des paysans.
