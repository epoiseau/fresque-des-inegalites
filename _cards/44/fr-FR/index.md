---
backDescription: >-
  Ainsi alors que la région de l’Afrique de l’Ouest compte autant de têtes de
  bétail que l’UE, la production de lait locale ne peut plus être écoulée dans
  les laiteries locales. En effet, celles-ci préfèrent importer de la poudre de
  lait européen. Produits en UE (Irlande, Pays-Bas, Pologne, Belgique, France)
  et incorporant de l’huile de palme, ces mélanges gras et pauvres en protéines
  sont vendus à bas prix à cause de la politique agricole commune. La production
  laitière pastorale ne peut pas lutter et se retrouve sous-employée. Elle a
  pourtant une meilleure qualité nutritionnelle, des impacts plus faibles sur
  l’environnement et le climat, et elle est une source de revenu pour l’économie
  locale.
title: >-
  Les subventions à l’agriculture dans les pays développés, notamment l’Europe,
  financent l’exportation de produits au détriment de l’agriculture locale.
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_44'
---
Exemple, le cas du coton d’Afrique de l’Ouest. Le coût de production de la livre de coton aux Etats-Unis est de 0,7 dollar, mais les subventions versées permettent de le réduire à 0,4 dollar, alors qu’il est de 0,5 dollar en Afrique. Cette concurrence déloyale est catastrophique pour les pays dépendants des recettes d’exportation tirées du coton : en 2002, le Mali, qui compte 3 millions de producteurs de coton, a réalisé une production équivalente à 50% de son potentiel.
