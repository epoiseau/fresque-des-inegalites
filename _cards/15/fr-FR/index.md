---
backDescription: >-
  Au moins la moitié des 7,6 milliards de personnes dans le monde n’a pas accès
  aux soins de première nécessité requis même en temps normal.
title: La moitié de la population du monde n’a pas accès aux soins
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_15'
---
En 2017, les dépenses de santé par personne dans les pays à faible revenu était 70 fois inférieures à celles des pays à revenu élevé, selon l'OMS. 
