---
backDescription: >-
  Le taux maximum moyen de l’impôt sur le revenu des particuliers aux États-Unis
  a presque diminué de moitié depuis 1980, passant de 70 % à 37 %.
title: Baisse du taux d'imposition des plus riches
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_03'
---
D'après T. Piketty (2014), Le Capital au XXIᵉ siècle. Le taux maximum moyen de l’impôt sur le revenu des particuliers aux États-Unis a presque diminué de moitié depuis 1980, passant de 70 % à 37 %.
