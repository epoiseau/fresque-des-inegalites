---
backDescription: >-
  En 2019, 637 millions de travailleurs et travailleuses dans le monde – soit
  près de 1 personne employée sur 5, ou 19 % – ne gagnaient pas assez pour se
  sortir eux-mêmes et leur famille de la pauvreté extrême ou modérée,
  c’est-à-dire qu’ils gagnaient moins de 3,20 dollars US par jour en parité de
  pouvoir d’achat (PPA).
title: Un travailleur sur cinq dans le monde est en situation de pauvreté
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_24'
---
Avoir un travail ne garantit pas un revenu décent, plus particulièrement dans les pays à revenu faible et intermédiaire.
