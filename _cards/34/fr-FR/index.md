---
backDescription: >-
  Les civils sont devenus les premières victimes des conflits armés dans le
  monde, en complète violation du droit humanitaire international.


  La proportion serait de 80 à 90 % selon l’ONU.


  Les infrastructures civiles sont régulièrement attaquées : routes, hôpitaux,
  écoles, marchés, réservoirs d’eau potable, exploitations agricoles, condamnant
  les populations à la faim ou à la migration.
title: Victimes civiles des conflits armés
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_34'
---
Selon Care France, les civils sont devenus les premières victimes des conflits armés dans le monde, en complète violation du droit humanitaire international. 

La proportion serait de 80 à 90 % selon l’ONU.
