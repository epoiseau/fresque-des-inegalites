---
backDescription: >-
  En France et dans le monde, les audiences de masse sont captives d’entreprises
  de presse largement contrôlées par des intérêts financiers, ce qui nuit à la
  pluralité des opinions diffusées dans le débat public.


  11 milliardaires français détiennent aujourd’hui plus de 80% des diffusions de
  titres de presse nationale, 57% des parts d’audience TV et 47% des parts
  d’audience radios.
title: Les médias d’opinion dépendent des intérêts industriels et financiers
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_20'
---
La concentration des médias entre les mains de quelques milliardaires nuit à la pluralité des opinions. Ces milliardaires utilisent leurs médias pour promouvoir leurs intérêts industriels ou politiques, ce qui limite le débat public et favorise les points de vue favorables aux affaires, au détriment de celles susceptibles de réduire les inégalités.
