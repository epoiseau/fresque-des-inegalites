---
backDescription: >-
  Les groupes marginalisés sont davantage susceptibles d’être victimes de
  maladies ou d’accidents.
title: 'Conditions de vie, promiscuité, alimentation, travail dangereux'
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_28'
---
Les groupes marginalisés ont tendance à occuper les emplois les plus dangereux et à plus faible salaire, et de vivre dans des conditions de promiscuité ou d'insalubrité. Il en résulte une augmentation du risque de maladie et d'accident.
