---
backDescription: >-
  Les 10 % les plus riches de la planète sont responsables de plus de la moitié
  des émissions de CO2.
title: Inégalité des émissions de CO2
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_32'
---
Entre 1990 et 2015 les 10 % les plus riches de la population mondiale sont responsables de 52 % des émissions de carbone accumulées dans l’atmosphère, puisant de près d’un tiers dans le budget carbone mondial à respecter afin d’atteindre l’objectif de l’Accord de Paris pour limiter le réchauffement climatique à 1,5 °C. La plupart des ménages français se situent dans les deux derniers déciles du graphique pour les revenus et 2 à 3 déciles pour les émissions.
