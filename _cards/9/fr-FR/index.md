---
backDescription: >-
  Entre 1985 et 2019, le taux légal moyen mondial d'imposition des sociétés est
  passé de 49 % à 23 %, en grande partie en raison de la montée de la
  concurrence fiscale internationale.
title: >-
  Entre 1985 et 2019, le taux légal moyen d’imposition sur les sociétés dans le
  monde a chuté de 49 % à 23 %
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_09'
---
Entre 1985 et 2019, le taux légal moyen mondial d'imposition des sociétés est passé de 49 % à 23 %, en grande partie en raison de la montée de la concurrence fiscale internationale. 
