---
backDescription: >-
  On observe depuis 2010 une nette hausse de la taxe sur la valeur ajoutée
  (TVA), un impôt régressif, et dans le même temps une réduction du taux
  d’imposition pour les entreprises les plus riches et des particuliers les plus
  fortunés.
title: Transfert de l'impôt des entreprises aux ménages
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_10'
---
On observe depuis 2010 une nette hausse de la taxe sur la valeur ajoutée (TVA), un impôt régressif, et dans le même temps une réduction du taux d’imposition pour les entreprises les plus riches et des particuliers les plus fortunés dans la plupart des pays développés ou en développement.
