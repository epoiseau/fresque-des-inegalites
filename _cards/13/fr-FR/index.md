---
backDescription: >-
  Entre 2009 et 2018, les versements aux actionnaires du CAC 40 ont augmenté de
  70 %, la rémunération des PDG de 60 %. Dans le même temps, le salaire moyen
  n’a augmenté que de 20 %, et de seulement 12 % pour les salariés payés au
  SMIC.
title: >-
  Les écarts de rémunération entre actionnaires, dirigeants et employés se
  creusent
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_13'
---
Entre 2009 et 2018, la rémunération des PDG des entreprises du CAC 40 a augmenté de 60 %. Sur la même période, le salaire moyen du personnel de ces multinationales françaises a augmenté de seulement 20 %, et 12 % pour les salariés payés au SMIC.
