---
backDescription: >-
  Dans les pays en développement, les enfants des familles riches ont sept fois
  plus de chances d’aller au terme du cycle secondaire que ceux des familles
  pauvres.


  Même dans les pays riches, trois quarts seulement des enfants des familles les
  plus pauvres terminent le cycle secondaire, alors que dans les familles les
  plus riches, ce chiffre s’élève à 90 %.
title: Accès à l’éducation
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_22'
---
Les enfants de familles riches ont plus de chances d'accéder à une éducation de qualité que les enfants de familles pauvres. Ces inégalités d'accès à l'éducation constituent un obstacle à la mobilité sociale et à la cohésion sociale.
