import os
import sys
file_path = os.path.realpath(__file__)

script_dir = os.path.dirname(file_path)
sys.path.insert(0, os.path.join(script_dir, '../memo-viewer/scripts/utils'))
from cardsmanipulation import split_pdf, export_and_compress_svg, convert_pdf_to_images

def _asset(filename):
    return os.path.join(script_dir, 'assets', filename)

if __name__ == '__main__':

    if len(sys.argv) != 2:
        print('python3 generate_cards.py <path_to_pdf_folder>')
        sys.exit(1)

    rename_map = { }

    replace_map = [
        (_asset('busts.png'), _asset('busts.svg'),  0.90),
        (_asset('bustslogo.png'), _asset('bustslogo.svg'),  0.84),
        (_asset('card.png'), _asset('card.svg'),  0.90),
        (_asset('circlelogo.png'), _asset('circlelogo.svg'),  0.86),
        (_asset('text.png'), _asset('text.svg'),  0.86),
    ]
    for k in range(1, 46):
        rename_map[str(-1 + 2*k)] = f'{k}-front'
        rename_map[str(0 + 2*k)] = f'{k}-back'

    path_to_pdf_folder = sys.argv[1]
    cards_folder = os.path.join(script_dir, '..', 'cards')
    if not os.path.exists(cards_folder):
        os.mkdir(cards_folder)

    warnings = []
    languages = os.listdir(path_to_pdf_folder)
    for pdf_filename in languages:
        language = pdf_filename[:-4]

        path_to_pdf = os.path.join(path_to_pdf_folder, pdf_filename)

        result_folder = os.path.join(cards_folder, language)
        if os.path.exists(result_folder):
            warnings.append(
                f'Skip language { language } as folder already exists')
            continue
        print(f'> Generate language { language }')
        os.mkdir(result_folder)
        pdf_folder = os.path.join(result_folder, 'pdf')
        print('>> Split pdf')
        split_pdf(path_to_pdf, rename_map, pdf_folder)

        print('>> Export and compress SVG')
        export_and_compress_svg(pdf_folder, image_replacements_filenames=replace_map)

        print('>> Convert to images')
        convert_pdf_to_images(pdf_folder)

    print('WARNINGS')
    for warning in warnings:
        print('* ' + warning)
